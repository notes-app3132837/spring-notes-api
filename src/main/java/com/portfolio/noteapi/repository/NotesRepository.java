package com.portfolio.noteapi.repository;

import com.portfolio.noteapi.entity.Notes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotesRepository extends JpaRepository<Notes, Long> {
}
