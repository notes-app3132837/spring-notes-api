package com.portfolio.noteapi.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="notes_tbl")
public class Notes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(
            name="id",
            updatable = false
    )
    private Long id;

    @Column(
            name="body",
            columnDefinition="TEXT"
    )
    private String body;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(
            name="createdAt",
            columnDefinition = "TIMESTAMP"
    )
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(
            name = "updatedAt",
            columnDefinition = "TIMESTAMP"
    )
    private Date updatedAt;
}
