package com.portfolio.noteapi.service;

import com.portfolio.noteapi.DTO.NoteRequest;
import com.portfolio.noteapi.DTO.NoteResponse;
import com.portfolio.noteapi.entity.Notes;
import com.portfolio.noteapi.repository.NotesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@RequiredArgsConstructor
public class NotesService {
    private final NotesRepository notesRepository;

    public Iterable<NoteResponse> getNotes(){
        return notesRepository.findAll().stream().map(this::mapToNoteResponse).toList();
    }

    public NoteResponse getNote(Long id){
        return mapToNoteResponse(notesRepository.findById(id).orElseThrow());
    }

    public String addNote(NoteRequest request){
        long currentTimeMillis = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(currentTimeMillis);
        Notes note = Notes.builder()
                .body(request.getBody())
                .createdAt(timestamp)
                .updatedAt(timestamp)
                .build();
        notesRepository.save(note);
        return "Success";
    }

    public String updateNote(NoteRequest request){
        long currentTimeMillis = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(currentTimeMillis);
        Notes note = notesRepository.findById(request.getId()).orElseThrow();
        note.setBody(request.getBody());
        note.setUpdatedAt(timestamp);
        notesRepository.save(note);
        return "Success";
    }

    public String deleteNote(Long id){
        Notes note = notesRepository.findById(id).orElseThrow();
        notesRepository.delete(note);
        return "Success";
    }

    private NoteResponse mapToNoteResponse(Notes note){
        return NoteResponse.builder()
                .id(note.getId())
                .body(note.getBody())
                .createdAt(note.getCreatedAt())
                .updatedAt(note.getUpdatedAt())
                .build();
    }

}
