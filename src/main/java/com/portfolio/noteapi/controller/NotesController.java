package com.portfolio.noteapi.controller;

import com.portfolio.noteapi.DTO.NoteRequest;
import com.portfolio.noteapi.DTO.NoteResponse;
import com.portfolio.noteapi.service.NotesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "api/v1/notes")
@CrossOrigin(origins = "${ALLOWED_ORIGINS}")
public class NotesController {
    private final NotesService notesService;

    @GetMapping
    public ResponseEntity<Iterable<NoteResponse>> getNotes(){
        return ResponseEntity.ok(notesService.getNotes());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<NoteResponse> getNote(@PathVariable Long id){
        return ResponseEntity.ok(notesService.getNote(id));
    }

    @PostMapping
    public ResponseEntity<String> createNote(@RequestBody NoteRequest request){
        return ResponseEntity.ok(notesService.addNote(request));
    }

    @PutMapping
    public ResponseEntity<String> updateNote(@RequestBody NoteRequest request){
        return ResponseEntity.ok(notesService.updateNote(request));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<String> deleteNote(@PathVariable Long id){
        return ResponseEntity.ok(notesService.deleteNote(id));
    }
}
